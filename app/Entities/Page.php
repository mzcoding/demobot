<?php
/**
 * Created by GreenCodeDigital Inc.
 * User: Mzcoding <mzcoding@gmail.com>
 */

namespace App\Entities;


use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
   use CrudTrait;

   protected $table = "pages";
   protected $primaryKey = "id";

   protected $fillable = [
       'title',
       'alias',
       'meta_description',
       'meta_tags',
       'content_header',
       'content_footer',
       'content'
   ];

   protected $dates = [
       'created_at',
       'updated_at'
   ];
}