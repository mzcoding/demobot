<?php

namespace App\Entities;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    use CrudTrait;

    protected $table = "deals";
    protected $primaryKey = "id";

    protected $guarded  = ['id'];
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'type',
        'payment_id',
        'price',
        'fork',
        'trader_id'
    ];

    //relations
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function trader()
    {
        return $this->hasOne(Trader::class, 'id', 'trader_id');
    }
    public function bank()
    {
        return $this->hasOne(Payment::class,  'id', 'payment_id');
    }
}
