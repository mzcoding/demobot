<?php

namespace App\Entities;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use CrudTrait;

    protected $table = "faq";
    protected $primaryKey = "id";

    protected $guarded  = ['id'];
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'answer',
        'question',
    ];


}
