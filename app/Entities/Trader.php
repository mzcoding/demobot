<?php

namespace App\Entities;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Trader extends Model
{
    use CrudTrait;

    protected $table = "traders";
    protected $primaryKey = "id";

    protected $guarded  = ['id'];
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'login_bot',
        'login_telegram',
        'balance',
        'rate',
        'quantity_deals'
    ];
}
