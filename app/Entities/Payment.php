<?php

namespace App\Entities;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use CrudTrait;

    protected $table = "payments";
    protected $primaryKey = "id";

    protected $fillable = [
        'payment',
        'banks_requisites',
        'status'
    ];
}
