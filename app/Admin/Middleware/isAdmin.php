<?php
/**
 * Created by GreenCodeDigital Inc.
 * User: Mzcoding <mzcoding@gmail.com>
 */

namespace App\Admin\Middleware;


use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!auth()->user()->isAdmin) {
            return redirect('/account');
        }

        return $next($request);
    }
}