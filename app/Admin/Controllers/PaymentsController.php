<?php


namespace App\Admin\Controllers;


use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

class PaymentsController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Entities\Payment');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/payments');
        $this->crud->setEntityNameStrings('банк', 'банк');

        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);


        $this->crud->setFromDb();


        $this->crud->setColumnDetails('id', ['label' => '#']);
        $this->crud->setColumnDetails('payment', ['label' => 'Банк']);
        $this->crud->setColumnDetails('banks_requisites', ['label' => 'Реквизиты']);
        $this->crud->setColumnDetails('status', ['label' => 'Активен', 'type' => 'boolean']);



        //$this->crud->removeColumns([]);
        $this->crud->addField([
            'name' => 'payment',
            'label' => "Банк",
            'attributes' => [
                'required' => 'required'
            ]
        ]);



        $this->crud->addField([
            'name' => 'status',
            'type' => 'checkbox',
            'label' => "Активен",
        ]);
        $this->crud->addField([
            'name' => 'banks_requisites',
            'type' => 'textarea',
            'label' => "Реквизиты",
        ]);



    }
    public function store(Request $request)
    {
        return parent::storeCrud($request);
    }

    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}