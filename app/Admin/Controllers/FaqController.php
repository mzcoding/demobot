<?php


namespace App\Admin\Controllers;


use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

class FaqController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Entities\Faq');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/faq');
        $this->crud->setEntityNameStrings('faq', 'faq');

        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);


        $this->crud->setFromDb();


        $this->crud->setColumnDetails('id', ['label' => '#']);
        $this->crud->setColumnDetails('question', ['label' => 'Вопрос']);
        $this->crud->setColumnDetails('answer', ['label' => 'Ответ']);


        //$this->crud->removeColumns([]);
        $this->crud->addField([
            'name' => 'question',
            'label' => "Вопрос",
            'attributes' => [
                'required' => 'required'
            ]
        ]);



        $this->crud->addField([
            'name' => 'answer',
            'type' => 'ckeditor',
            'label' => "Ответ",
        ]);



    }
    public function store(Request $request)
    {
        return parent::storeCrud($request);
    }

    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}