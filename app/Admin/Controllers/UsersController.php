<?php

namespace App\Admin\Controllers;


use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

class UsersController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Entities\User');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/users');
        $this->crud->setEntityNameStrings('админа', 'админы');

        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);


        $this->crud->setFromDb();


        $this->crud->setColumnDetails('id', ['label' => '#']);
        $this->crud->setColumnDetails('email', ['label' => 'E-mail админа']);
        $this->crud->setColumnDetails('name', ['label' => 'Имя админа']);


        //$this->crud->removeColumns([]);
        $this->crud->addField([
            'name' => 'name',
            'label' => "Имя администратора",
            'attributes' => [
                'required' => 'required'
            ]
        ]);

        $this->crud->addField([
            'name' => 'email',
            'label' => "E-mail администратора",
            'attributes' => [
                'required' => 'required'
            ]
        ]);


        $this->crud->addField([
            'name'  => 'password',
            'type'  => 'password',
            'label' => "Пароль администратора",
            'attributes' => [
                'required' => 'required'
            ]
        ]);

        $this->crud->addField([
            'name' => 'isAdmin',
            'type' => 'checkbox',
            'label' => "Администратор",
            'attributes' => [
                'checked' => 'checked'
            ]
        ]);



        $this->crud->removeColumn('remember_token');

    }
    public function store(Request $request)
    {
        return parent::storeCrud($request);
    }

    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}