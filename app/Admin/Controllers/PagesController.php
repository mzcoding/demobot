<?php

namespace App\Admin\Controllers;

use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class PagesController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Entities\Page');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/pages');
        $this->crud->setEntityNameStrings('страницу', 'страницы');

        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);


        $this->crud->setFromDb();


        $this->crud->setColumnDetails('id', ['label' => '#']);
        $this->crud->setColumnDetails('title', ['label' => 'Наименование страницы']);
        $this->crud->setColumnDetails('alias', ['label' => 'Alias']);

        $this->crud->removeColumns([
            'meta_description', 'meta_tags',
            'content_header','content_footer','content'
        ]);

        $this->crud->addField([
            'name' => 'title',
            'label' => "Наименование",
            'attributes' => [
                'required' => 'required'
            ]
        ]);

        $this->crud->addField([
            'name' => 'alias',
            'label' => "Alias",
            'attributes' => [
                'required' => 'required'
            ]
        ]);
        $this->crud->addField([
            'name' => 'meta_description',
            'label' => "описание (meta)",
        ]);

        $this->crud->addField([
            'name' => 'meta_tags',
            'label' => "теги (meta)",
        ]);


        $this->crud->addField([
            'name' => 'content_header',
            'type' => 'ckeditor',
            'label' => "Контент в шапку",

        ]);
        $this->crud->addField([
            'name' => 'content_footer',
            'type' => 'ckeditor',
            'label' => "Контент в подвал",

        ]);
        $this->crud->addField([
            'name' => 'content',
            'type' => 'ckeditor',
            'label' => "Контент",
            'attributes' => [
                'required' => 'required'
            ]
        ]);

    }
    public function store(Request $request)
    {
        return parent::storeCrud($request);
    }

    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}
