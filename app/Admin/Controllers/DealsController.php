<?php


namespace App\Admin\Controllers;


use App\Entities\Payment;
use App\Entities\Trader;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

class DealsController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Entities\Deal');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/deals');
        $this->crud->setEntityNameStrings('сделку', 'сделки');

        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);


        $this->crud->setFromDb();
        $this->crud->addFilter([
            'name' => 'type',
            'type' => 'dropdown',
            'label'=> 'Тип сделки'
        ], [
            0 => 'Продажа',
            1 => 'Покупка'
        ], function($value) { // if the filter is active
             $this->crud->addClause('where', 'type', $value);
        });


        $this->crud->setColumnDetails('id', ['label' => '#']);
        $this->crud->setColumnDetails('type', [
                 'label' => 'Тип сделки', 'type' => 'boolean',
                 'options' => [0 => 'продажа', 1 => 'покупка']]);
        $this->crud->setColumnDetails('trader_id', [
                 'label' => 'Продавец/Покупатель', 'type' => 'select',
                 'name' => 'trader', 'entity' => 'trader',
                 'attribute' => 'login_bot', 'model' => 'App\Entities\Deal']);
        $this->crud->setColumnDetails('fork', ['label' => 'Вилка цен']);
        $this->crud->setColumnDetails('price', [
            'label' => 'Цена курса', 'type' => 'number', 'decimals' => 2, 'prefix' => 'btc']);
        $this->crud->setColumnDetails('payment_id',
             ['label' => 'Банк', 'type' => 'select', 'name' => 'bank', 'entity' => 'bank',
                 'attribute' => 'payment', 'model' => 'App\Entities\Deal']);

        //$this->crud->removeColumns([]);
        $this->crud->addField([
            'name' => 'type',
            'type' => 'select_from_array',
            'options' => [0 => 'Продажа', 1 => 'Покупка'],
            'label' => "Тип сделки",
            'attributes' => [
                'required' => 'required'
            ]
        ]);



        $banks = (new Payment())->get();
        $bankList = [];
        foreach ($banks as $bank) {
            $bankList[$bank->id] = $bank->payment;
        }
        $this->crud->addField([
            'name' => 'payment_id',
            'label' => "Банк",
            'type'  => 'select_from_array',
            'options' => $bankList,
           // 'entity' => 'payment',
            //'attribute' => 'name',
           // 'model' => 'App\Entities\Deal'
        ]);
        $traders = (new Trader())->get();
        $traderList = [];
        foreach($traders as $trader) {
            $traderList[$trader->id] = $trader->login_bot . "(" . $trader->balance . " btc)";
        }
        $this->crud->addField([
            'name' => 'trader_id',
            'label' => "Трейдер",
            'type'  => 'select_from_array',
            'options' => $traderList,
        ]);

        $this->crud->addField([
            'name' => 'price',
            'label' => "Стоимость",
        ]);



        $this->crud->addField([
            'name' => 'fork',
            'type' => 'text',
            'label' => "Диапазон цен",
            'attributes' => [
                'required' => 'required',
                'placeholder' => '10-100'
            ]
        ]);

    }
    public function store(Request $request)
    {
        return parent::storeCrud($request);
    }

    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}