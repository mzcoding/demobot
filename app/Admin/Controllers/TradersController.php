<?php
/**
 * Created by GreenCodeDigital Inc.
 * User: Mzcoding <mzcoding@gmail.com>
 */

namespace App\Admin\Controllers;


use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request;

class TradersController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Entities\Trader');
        $this->crud->setRoute(config('backpack.base.route_prefix')  . '/traders');
        $this->crud->setEntityNameStrings('трейдера', 'трейдеры');

        $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);


        $this->crud->setFromDb();


        $this->crud->setColumnDetails('id', ['label' => '#']);
        $this->crud->setColumnDetails('login_bot', ['label' => 'Логин в боте']);
        $this->crud->setColumnDetails('login_telegram', ['label' => 'Логин в телеграме']);
        $this->crud->setColumnDetails('balance', ['label' => 'Баланс BTC']);
        $this->crud->setColumnDetails('rate', ['label' => 'Рейтинг']);
        $this->crud->setColumnDetails('quantity_deals', ['label' => 'Колличество сделок']);

        //$this->crud->removeColumns([]);
        $this->crud->addField([
            'name' => 'login_bot',
            'label' => "Логин в боте",
            'attributes' => [
                'required' => 'required'
            ]
        ]);

        $this->crud->addField([
            'name' => 'login_telegram',
            'label' => "Логин в телеграм",
            'attributes' => [
                'required' => 'required'
            ]
        ]);


        $this->crud->addField([
            'name' => 'balance',
            'label' => "Баланс (BTC)",
            'type'  => 'text',
            'attributes' => [
                'required' => 'required',
                'placeholder' => '0.22'
            ]
        ]);

        $this->crud->addField([
            'name' => 'rate',
            'label' => "Рейтинг",
        ]);



        $this->crud->addField([
            'name' => 'quantity_deals',
            'type' => 'number',
            'label' => "Колличество сделок",
            'attributes' => [
                'required' => 'required'
            ]
        ]);

    }
    public function store(Request $request)
    {
        return parent::storeCrud($request);
    }

    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}