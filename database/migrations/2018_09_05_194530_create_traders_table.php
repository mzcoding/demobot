<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login_bot', 191)->nullable();
            $table->string('login_telegram', 191)->nullable();
            $table->decimal('balance', 10, 2)->default(0); //balance btc
            $table->decimal('rate', 10, 2);
            $table->integer('quantity_deals')->default(0); //колл-во сделок
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traders');
    }
}
