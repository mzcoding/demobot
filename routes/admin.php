<?php

CRUD::resource('pages', 'PagesController');
CRUD::resource('traders', 'TradersController');
CRUD::resource('faq', 'FaqController');
CRUD::resource('users', 'UsersController');
CRUD::resource('deals', 'DealsController');
CRUD::resource('payments', 'PaymentsController');