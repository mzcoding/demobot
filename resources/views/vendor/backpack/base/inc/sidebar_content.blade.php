<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li class="treeview">
    <a href="">
        <i class="fa fa-deaf"></i>
        <span>Список сделок</span>
        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('deals') }}"><i class="fa fa-circle-o"></i> Покупка</a></li>
        <li class="active"><a href="{{ backpack_url('deals') }}"><i class="fa fa-circle-o"></i> Продажа</a></li>
    </ul>
</li>
<li><a href="{{ backpack_url('traders') }}"><i class="fa fa-trademark"></i> <span>
            Трейдеры</span></a></li>
<li><a href="{{ backpack_url('users') }}"><i class="fa fa-users"></i> <span>
            Админы</span></a></li>

<li><a href="none"><i class="fa fa-feed"></i> <span>
            Арбитраж</span></a></li>

<li><a href="{{ backpack_url('payments') }}"><i class="fa fa-bank"></i> <span>
            Банки</span></a></li>

<li><a href="{{ backpack_url('faq') }}"><i class="fa fa-question"></i> <span>
            FAQ</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li><a href="{{ backpack_url('pages') }}"><i class="fa fa-file"></i> <span>
            Страницы</span></a></li>

<li><a href='{{ url(config('backpack.base.route_prefix', 'admin') . '/setting') }}'><i class='fa fa-cog'></i>
        <span>Параметры</span></a></li>
